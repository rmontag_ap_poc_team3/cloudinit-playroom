(1) Create Azure Resource Group
(2) Create VM using cloud-init.txt:

az vm create --resource-group myResourceGroupAutomate --name myVM --image UbuntuLTS --admin-username azureuser --ssh-key-value @~/.ssh/id_rsa.pub --custom-data cloud-init.txt

(3) Open Port 80:
az vm open-port --port 80 --resource-group myResourceGroupAutomate --name myVM

(4) http://51.4.X.Y 
Nginx will answer.

Keyfault:

(a) Create Keyfault

λ az keyvault create --resource-group ex532ResourceGroup --name ex532keyvault --enabled-for-deployment

(b) Create self-signert certificate

λ az keyvault certificate get-default-policy > defaultpolicy.json

λ az keyvault certificate create --vault-name ex532keyvault --name ex532cert --policy @defaultpolicy.json

(c) obtain id of certificat from keyvault

λ az keyvault secret list-versions --vault-name ex532keyvault --name ex532cert --query "[?attributes.enabled].id" --output tsv
https://ex532keyvault.vault.microsoftazure.de/secrets/ex532cert/1a4dee111acf4187845bbac350d3bf14

λ az vm format-secret --secret https://ex532keyvault.vault.microsoftazure.de/secrets/ex532cert/1a4dee111acf4187845bbac350d3bf14 >vm_secret.txt

λ az vm create --resource-group ex532ResourceGroup --name ex532VM6 --image UbuntuLTS --admin-username admin001 --ssh-key-value @~/.ssh/id_rsa.pub --custom-data cloud-init-secured.txt --secrets @vm_secret.txt

λ az vm open-port --port 443 --resource-group ex532ResourceGroup --name ex532VM6
                                                                               
Jenkins:

(i) create VM
λ az vm create --resource-group ex532ResourceGroup --name ex532VM7 --image UbuntuLTS --admin-username admin001 --ssh-key-value @~/.ssh/id_rsa.pub --custom-data cloud-init-jenkins.txt

(ii) open ports
λ az vm open-port --port 8080 --priority 1001 --resource-group ex532ResourceGroup --name ex532VM7
λ az vm open-port --port 1337 --priority 1002 --resource-group ex532ResourceGroup --name ex532VM7

(iii) determine public ip
λ az vm show --name ex532VM7 --resource-group ex532ResourceGroup -d --query [publicIps] --out tsv
 
(iv) SSH
$ ssh -i /cygdrive/c/Users/MasterPlan-S03/.ssh/id_rsa admin001@51.4.202.42

(v) retrieve initialPassword
$ sudo cat /var/lib/jenkins/secrets/initialAdminPassword

(vi) Configure Jenkins, BitBucket ...




